/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.intercept('GET', '/todos/*', {fixture: 'example'}).as('todo');
  });

  describe('Example test', () => {
    it('Does not do much!', () => {
      cy.visit('http://localhost:4200');
      expect(true).to.equal(true);
      cy.get('.mat-card > :nth-child(2)').contains('PLOP');
    });
  });
});
