# Docker

The base image used comes from Cypress.

The idea is getting an image with node and cypress already installed for running the tests inside your container.

# Docker-compose

The docker-compose file is a simple one using the Dockerfile in this directory and setting configured .env ports.

