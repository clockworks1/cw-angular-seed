import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {DemoService} from './demo.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {TodoInterface} from '../../core/models/todo.model';

@Injectable()
export class DemoResolver implements Resolve<any> {

  constructor(private demoService: DemoService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.demoService.getTodo(1).pipe(
      catchError(err => {
        // Do something...
        return err;
      })
    );
  }
}
