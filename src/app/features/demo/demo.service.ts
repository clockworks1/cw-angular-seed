import {Injectable} from '@angular/core';
import {ApiJsonService} from '../../core/api/api-json.service';
import {catchError, map} from 'rxjs/operators';
import {SharedService} from '../../shared/services/shared.service';
import {Observable, of, throwError} from 'rxjs';
import {TodoInterface} from '../../core/models/todo.model';


@Injectable()
export class DemoService {

  constructor(private apiJsonService: ApiJsonService,
              private sharedService: SharedService) {
  }

  getTodo(id: number): Observable<TodoInterface> {
    return this.apiJsonService.getTodo(id).pipe(
      map(todo => todo),
      catchError(err => {
        this.sharedService.openSnackbar(err.message || JSON.stringify(err));
        return throwError(err);
      })
    );
  }

}
