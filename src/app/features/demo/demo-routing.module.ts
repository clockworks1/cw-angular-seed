import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DemoComponent} from './demo.component';
import {DemoSubrouteComponent} from './demo-subroute/demo-subroute.component';
import {DemoResolver} from './demo.resolver';


const routes: Routes = [{
  path: '',
  component: DemoComponent,
  resolve: {
    todo: DemoResolver,
  },
  children: [
    {
      path: '',
      redirectTo: 'subroute',
    },
    {
      path: 'subroute',
      component: DemoSubrouteComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule {
}
