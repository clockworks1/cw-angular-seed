import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {DemoComponent} from './demo.component';
import {DemoRoutingModule} from './demo-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {DemoSubrouteComponent} from './demo-subroute/demo-subroute.component';
import {DemoService} from './demo.service';
import {DemoResolver} from './demo.resolver';


@NgModule({
  declarations: [
    DemoComponent,
    DemoSubrouteComponent,
  ],
  imports: [
    CommonModule,
    DemoRoutingModule,
    SharedModule,
  ],
  providers: [
    DemoService,
    DemoResolver,
  ]
})
export class DemoModule {
}
