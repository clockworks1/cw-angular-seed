import {AfterViewInit, Component, OnInit} from '@angular/core';
import {SharedService} from '../../shared/services/shared.service';
import {ActivatedRoute} from '@angular/router';
import {TodoInterface} from '../../core/models/todo.model';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit, AfterViewInit {

  todo: TodoInterface;

  constructor(private route: ActivatedRoute,
              private sharedService: SharedService) {
    this.route.data.subscribe(data => {
      if (data.todo) {
        this.todo = data.todo;
      }
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.sharedService.openSnackbar('HELLO WORLD');
  }

}
