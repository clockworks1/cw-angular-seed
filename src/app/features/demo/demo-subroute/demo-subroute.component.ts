import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-demo-subroute',
  templateUrl: './demo-subroute.component.html',
  styleUrls: ['./demo-subroute.component.scss']
})
export class DemoSubrouteComponent implements OnInit {

  pipeExample: string;

  constructor() {
    this.pipeExample = 'Will be transformed on display';
  }

  ngOnInit(): void {
  }

}
