import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DemoSubrouteComponent} from './demo-subroute.component';
import {SharedModule} from '../../../shared/shared.module';

describe('DemoSubrouteComponent', () => {
  let component: DemoSubrouteComponent;
  let fixture: ComponentFixture<DemoSubrouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemoSubrouteComponent],
      imports: [SharedModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSubrouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
