import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TodoInterface} from '../models/todo.model';
import {Observable} from 'rxjs';

@Injectable()
export class ApiJsonService {

  baseUrl = 'https://jsonplaceholder.typicode.com/';

  constructor(private httpClient: HttpClient) {
  }

  getTodo(id: number): Observable<TodoInterface> {
    return this.httpClient.get<TodoInterface>(`${this.baseUrl}todos/${id}`);
  }
}
