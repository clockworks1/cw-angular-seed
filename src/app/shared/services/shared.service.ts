import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private snackBar: MatSnackBar) {
  }

  openSnackbar(message: string, action?: string): void {
    this.snackBar.open(message, action || `Dismiss`, {
      duration: 30000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom'
    });
  }
}
