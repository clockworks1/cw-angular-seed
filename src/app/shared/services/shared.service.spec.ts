import { TestBed } from '@angular/core/testing';

import { SharedService } from './shared.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';

describe('SharedService', () => {
  let service: SharedService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
    });
    service = TestBed.inject(SharedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
