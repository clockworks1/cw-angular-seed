import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {ExamplePipe} from './pipes/example.pipe';
import {MatSnackBarModule} from '@angular/material/snack-bar';

const MATERIAL_MODULES = [
  MatListModule,
  MatSidenavModule,
  MatCardModule,
  MatSnackBarModule,
];

@NgModule({
  declarations: [
    ExamplePipe
  ],
  exports: [
    ...MATERIAL_MODULES,
    ExamplePipe,
  ],
  imports: [
    CommonModule,
    ...MATERIAL_MODULES,
  ],
})

export class SharedModule {
}
